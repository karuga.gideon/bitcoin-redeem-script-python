import base58
import hashlib
from bitcoin import * 
from bitcoinlib.services.services import * 
from bitcoinlib.wallets import Wallet
from bitcoinlib.transactions import Transaction

def generate_redeem_script(preimage):
    # Calculate SHA-256 hash of the preimage
    preimage_hash = hashlib.sha256(preimage.encode()).digest()

    # Convert the preimage hash to hexadecimal format
    lock_hex = preimage_hash.hex()

    # Construct the redeem script in hex format
    redeem_script_hex = 'OP_SHA256 {} OP_EQUAL'.format(lock_hex)

    return lock_hex, redeem_script_hex

def derive_address(redeem_script_hex):
    # Hash the redeem script using SHA256 algorithm
    redeem_script_hash = hashlib.sha256(redeem_script_hex.encode()).digest()

    # Hash the result using the RIPEMD160 algorithm
    ripemd_hash = hashlib.new('ripemd160', redeem_script_hash).digest()

    # Append the appropriate network byte (Bitcoin mainnet or testnet)
    network_byte = b'\x6f'  # Assuming testnet, change to b'\x00' for mainnet
    hashed_data = network_byte + ripemd_hash

    # Perform Base58Check encoding
    address = base58.b58encode_check(hashed_data)

    return address.decode()

def construct_transaction(address, amount):
    # amount is in satoshis
    # tx = Transaction(fee=5000, outputs=[Output(amount, address)], network='testnet')
    tx = Transaction(network='regtest')
    prev_hash = '9c81f44c29ff0226f835cd0a8a2f2a7eca6db52a711f8211b566fd15d3e0e8d4'
    tx.add_input(prev_hash, output_n=0)
    # tx.add_output(amount, address)
    tx.info()
    
    print(tx.verify())
    print(tx.info())
    rawhextx = tx.raw_hex()
    # tx = Service().sendrawtransaction(rawhextx)
    print(rawhextx)
    print(tx)
    return tx

def create_signed_transaction(txin, txout, txin_scriptPubKey, txin_scriptSig):
    tx = CMutableTransaction([txin], [txout])
    txin.scriptSig = CScript(txin_scriptSig)
    VerifyScript(txin.scriptSig, CScript(txin_scriptPubKey),
                 tx, 0, (SCRIPT_VERIFY_P2SH,))
    return tx

def construct_spending_transaction(previous_tx, redeem_script_hex, unlock_script_hex, amount_to_send, fee):
    # Create a transaction that spends from the previous transaction    
    tx = Transaction.deserialize(previous_tx)
    print(tx)

    # Add an output to send bitcoins to a new address
    new_address = sha256('new_address'.encode()).digest()
    new_address_hex = bytes_to_hexstring(new_address)
    new_output = (new_address_hex, amount_to_send)
    tx['outs'].append(new_output)
    
    # Create the input script
    unlocking_script = hexstring_to_bytes(unlock_script_hex)
    tx['ins'][0]['script'] = unlocking_script
    
    # Sign the transaction
    tx = sign(tx, 0, redeem_script_hex)
    
    # Calculate transaction fee
    tx_size = len(serialize(tx)) // 2  # Size in bytes
    tx_fee = fee * tx_size
    
    # Reduce the output amount by the fee
    tx['outs'][0][1] -= tx_fee
    
    return serialize(tx)

def test_functions():
    preimage = "Btrust Builders"
    lock_hex, redeem_script = generate_redeem_script(preimage)
    print("Redeem Script:", redeem_script)

    address = derive_address(lock_hex)
    print("Derived Address:", address)

    tx = construct_transaction(address, 100000)
    print("Constructed Transaction:", tx)

    previous_tx = '01000000000101aaabababababababababababababababababababababababababababababababab0000000000ffffffff0200e1f505000000001976a914111111111111111111111111111111111111111188ac001b0f01e000000001976a914111111111111111111111111111111111111111188ac00000000'
    redeem_script_hex = 'OP_SHA256 1111111111111111111111111111111111111111111111111111111111111111 OP_EQUAL'
    unlock_script_hex = '0000'
    spending_tx = construct_spending_transaction(previous_tx, redeem_script_hex, unlock_script_hex, 90000, 1)
    print("Spending Transaction:", spending_tx)

if __name__ == "__main__":
    test_functions()
