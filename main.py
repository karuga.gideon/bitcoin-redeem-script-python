import base58
import hashlib
import json
import sys

path_to_bitcoin_functional_test = "/Users/gideon/bitcoin/bitcoin/test/functional"
path_to_bitcoin_tx_tutorial = "/Users/gideon/bitcoin/bitcoin-tx-tutorial"

# Add the functional test framework to our PATH
sys.path.insert(0, path_to_bitcoin_functional_test)
from test_framework.test_shell import TestShell

# Add the bitcoin-tx-tutorial functions to our PATH
sys.path.insert(0, path_to_bitcoin_tx_tutorial)
from functions import *


# Setup our regtest environment
test = TestShell().setup(
    num_nodes=1, 
    setup_clean_chain=True
)

node = test.nodes[0]

def creaete_and_fund_wallet():
    # Create a new wallet and address to send mining rewards so we can fund our transactions
    node.createwallet(wallet_name='mywallet')
    address = node.getnewaddress()

    # Generate 101 blocks so that the first block subsidy reaches maturity
    result = node.generatetoaddress(nblocks=101, address=address, invalid_call=False)
    print(result)

    print(node.getblockcount())

    # Check that we were able to mine 101 blocks
    assert(node.getblockcount() == 101)

def generate_redeem_script(preimage):
    # Calculate SHA-256 hash of the preimage
    preimage_hash = hashlib.sha256(preimage.encode()).digest()

    # Convert the preimage hash to hexadecimal format
    lock_hex = preimage_hash.hex()

    # Construct the redeem script in hex format
    redeem_script_hex = 'OP_SHA256 {} OP_EQUAL'.format(lock_hex)

    return lock_hex, redeem_script_hex

def derive_address(redeem_script_hex):
    # Hash the redeem script using SHA256 algorithm
    redeem_script_hash = hashlib.sha256(redeem_script_hex.encode()).digest()

    # Hash the result using the RIPEMD160 algorithm
    ripemd_hash = hashlib.new('ripemd160', redeem_script_hash).digest()

    # Append the appropriate network byte (Bitcoin mainnet or testnet)
    network_byte = b'\x6f'  # Assuming testnet, change to b'\x00' for mainnet
    hashed_data = network_byte + ripemd_hash

    # Perform Base58Check encoding
    address = base58.b58encode_check(hashed_data)

    return address.decode()

def test_functions():
    preimage = "Btrust Builders"
    lock_hex, redeem_script = generate_redeem_script(preimage)
    print("Redeem Script:", redeem_script)

    address = derive_address(lock_hex)
    print("Derived Address:", address)
    
    receiver_spk = bytes.fromhex("76a9143bc28d6d92d9073fb5e3adf481795eaf446bceed88ac")

    # Create a new wallet and address to send mining rewards so we can fund our transactions
    node.createwallet(wallet_name='mywallet')
    address = node.getnewaddress()

    # Generate 101 blocks so that the first block subsidy reaches maturity
    node.generatetoaddress(nblocks=101, address=address, invalid_call=False)    
    # print("Mined blocks: ", node.getblockcount())
    txid_to_spend, index_to_spend = fund_address(node, address, 2.001)
    print(f"txid: {txid_to_spend}, {index_to_spend}")

    # Set our outputs
    # Create a new pubkey to use as a change output.
    change_privkey = bytes.fromhex("4444444444444444444444444444444444444444444444444444444444444444")
    change_pubkey = privkey_to_pubkey(change_privkey)

    # Determine our output scriptPubkeys and amounts (in satoshis)
    output1_value_sat = int(float("1.5") * 100000000)
    output1_spk = receiver_spk
    output2_value_sat = int(float("0.5") * 100000000)
    output2_spk = bytes.fromhex("76a914") + hash160(change_pubkey) + bytes.fromhex("88ac")

    # VERSION
    # version '2' indicates that we may use relative timelocks (BIP68)
    version = bytes.fromhex("0200 0000")

    # INPUTS
    # We have just 1 input
    input_count = bytes.fromhex("01")

    # Convert txid and index to bytes (little endian)
    txid = (bytes.fromhex(txid_to_spend))[::-1]
    index = index_to_spend.to_bytes(4, byteorder="little", signed=False)

    # For the unsigned transaction we use an empty scriptSig
    scriptsig = bytes.fromhex("")

    # use 0xffffffff unless you are using OP_CHECKSEQUENCEVERIFY, locktime, or rbf
    sequence = bytes.fromhex("ffff ffff")

    inputs = (
        txid
        + index
        + varint_len(scriptsig)
        + scriptsig
        + sequence
    )

    # OUTPUTS
    # 0x02 for out two outputs
    output_count = bytes.fromhex("02")

    # OUTPUT 1 
    output1_value = output1_value_sat.to_bytes(8, byteorder="little", signed=True)
    # 'output1_spk' already defined at the start of the script

    # OUTPUT 2
    output2_value = output2_value_sat.to_bytes(8, byteorder="little", signed=True)
    # 'output2_spk' already defined at the start of the script

    outputs = (
        output1_value
        + pushbytes(output1_spk)
        + output2_value
        + pushbytes(output2_spk)
    )

    # LOCKTIME
    locktime = bytes.fromhex("0000 0000")

    unsigned_tx = (
        version
        + input_count
        + inputs
        + output_count
        + outputs
        + locktime
    )

    print("Spending Transaction hex:", unsigned_tx.hex())

if __name__ == "__main__":
    test_functions()

