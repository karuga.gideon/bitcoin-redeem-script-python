## About

# This script does the following;

1. Generates the redeem script in hex format for the given pre-image. Note: redeem_script => OP_SHA256 <lock_hex> OP_EQUAL.
2. Derives an address with from the above (1) redeem script
3. Constructs a transaction that send Bitcoins to the address.
4. Constructs another transaction that spends from the above (3) transaction

![Execution screenshot](Screenshot.png)
